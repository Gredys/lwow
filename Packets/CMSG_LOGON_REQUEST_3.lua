---------------------------------------
--                                   --
---------------------------------------

	-- CMSG_LOGON_REQUEST_3 => 9 [Channels => 0]
	Hook.Add("OnPacket::9::0", "CMSG_LOGON_REQUEST_3", function(Client, Buffer)
		Client.Program = Buffer:ReadFourCC()
		Client.Platform = Buffer:ReadFourCC()
		Client.Locale = Buffer:ReadFourCC()
		Client.Components = {}

		for i=1, Buffer:Read(6) do
			Client.Components[i] = {
				Program = Buffer:ReadFourCC(),
				Platform = Buffer:ReadFourCC(),
				Build = Buffer:Read(32),
			}

			-- PrintTable(Client.Components[i])
		end

		Client.Login = Buffer:Read(1) ~= 0 and Buffer:ReadString(9, 3) or ""
		Client.Compatibility = Buffer:Read(64)

		Hook.Call("HandlePacket::9::0", Client)
	end)

	Hook.Add("HandlePacket::9::0", "CMSG_LOGON_REQUEST_3", function(Client)
		if Client.Login ~= "" then
			Client:Write(0, 6)		-- Opcode -> SMSG_LOGON_RESPONSE(0)
			Client:Write(1, 1)		-- unkChannel (always [1,1])
			Client:Write(0, 4)		-- Channel -> CONNECTION(0)



			Client:Write(1, 1) 				-- ResponseFailure
			Client:Write(0, 1)				-- !Modules.empty()
			Client:Write(1, 2)				-- ResultValue -> FAILURE (1)
			Client:Write(109, 16) 			-- AUTH_INVALID_PROGRAM (109)
			Client:Write(0, 32)	-- Wait?
			Client:Send()
		else

		end
	end)

---------------------------------------
--                                   --
---------------------------------------
