---------------------------------------
-- Bit stream                        --
---------------------------------------

	-- Helpers
	local function ConvertNumberToString(numb, len)
		local bytes = string.format("%08X", numb)
		local ret = {}
		for i=#bytes, 1 + (#bytes - len * 2), -2 do
			ret[#ret+1] = string.char(tonumber(bytes:sub(i-1, i), 16))
		end
		return table.concat(ret, "")
	end

	local function bswap(x)
		local a = (x) 		& 0xff
		local b = (x >> 8) 	& 0xff
		local c = (x >> 16) & 0xff
		local d = (x >> 24) & 0xff
		return (((a << 8) + b << 8) + c << 8) + d
	end

	-- Methods
	local mBitStream = {}

	function mBitStream:AlignToNextByte()
		self.ReadP = (self.ReadP + 7) & ~7
	end

	function mBitStream:Read(bitCount, swap)
		local ret = 0
		while bitCount > 0 do
			local bitPos = self.ReadP & 7
			local bitsLeft = 8 - bitPos
			if(bitsLeft > bitCount) then bitsLeft = bitCount end

			bitCount = bitCount - bitsLeft
			local tp = self.ReadP >> 3
			-- printf("-- %s[%s] => %s", #self.data, self.ReadP, tp + 1)
			ret = ret | (((self.data[tp + 1] >> bitPos) & (1 << bitsLeft) - 1) << bitCount)
			self.ReadP = self.ReadP + bitsLeft
		end
		return swap and bswap(ret) or ret
	end

	function mBitStream:ReadFourCC()
		local numb = self:Read(32, true)
		local len = 4
		while (numb & 0xFF) == 0 do
			numb = numb >> 8
			len = len - 1
		end
		return ConvertNumberToString(numb, len)
	end

	function mBitStream:ReadString(bits, length)
		local len = self:Read(bits) + length
		local str = {}
		self:AlignToNextByte()
		for i=1, len do
			local b = self.data[(self.ReadP >> 3) + i]
			str[#str + 1] = string.char(b)
		end
		self.ReadP = self.ReadP + len * 8
		return table.concat(str, "")
	end

	-- Constructor
	function BitStream(data)
		return Class({
			data = data,
			ReadP = 0,
		}, mBitStream)
	end

---------------------------------------
--                                   --
---------------------------------------
