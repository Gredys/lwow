---------------------------------------
--                                   --
---------------------------------------

	function Class(data, class, meta)
		for k,v in pairs(class) do
			data[k] = v
		end

		if meta then setmetatable(data, meta) end
		data.__index = data

		return data
	end

---------------------------------------
--                                   --
---------------------------------------
