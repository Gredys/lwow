---------------------------------------
-- Socket stream                     --
---------------------------------------

	-- TODO: Split by two files -> SocketServer/SocketClient

	-- Methods
	local mSocketStream = {}

	-- Getters/Setters
	function mSocketStream:GetSockName()
		return self.Socket:getsockname()
	end

	function mSocketStream:GetPeerName()
		return self.Socket:getpeername()
	end

	function mSocketStream:SetTimeOut(t)
		self.Socket:settimeout(t)
	end

	-- Other
	function mSocketStream:Accept()
		local clientSocket = self.Socket:accept()
		if clientSocket then
			return SocketStream(clientSocket)
		end
	end

	function mSocketStream:ReadToBuffer()
		local buffer = {}

		local char, err = self.Socket:receive(1)
		while char and not err do
			buffer[#buffer + 1] = char:byte()
			char, err = self.Socket:receive(1)
		end

		if #buffer > 0 then
			-- printf("Buffer -> %s", #buffer)
			return BitStream(buffer)
		elseif err then
			return nil, err
		end
	end

	-- Write
	function mSocketStream:Write(value, bitCount)
		while bitCount > 0 do

			local bitPos = self.WriteP & 7
			local bitsLeftInByte = 8 - bitPos
			if (bitsLeftInByte > bitCount) then
				bitsLeftInByte = bitCount
			end

			bitCount = bitCount - bitsLeftInByte

			local firstHalf = ~((((1 << bitsLeftInByte) - 1) << bitPos))
			local secondHalf = ((((1 << bitsLeftInByte) - 1) & (value >> bitCount)) << bitPos)

			local wp = (self.WriteP >> 3) + 1
			if #self.Buffer > wp - 1 then
				self.Buffer[wp] = ((self.Buffer[wp] or 0) & firstHalf) | secondHalf
			else
				self.Buffer[wp] = secondHalf
			end
			-- printf("-- %s[%s] -> %s = %s (%02X)", #self.Buffer, self.WriteP, wp, self.Buffer[wp], self.Buffer[wp])

			self.WriteP = self.WriteP + bitsLeftInByte
		end
	end

	function mSocketStream:Send()
		local buffer = {}
		local hex = {}
		for i=1, #self.Buffer do
			buffer[i] = string.char(self.Buffer[i])
			hex[i] = string.format("%02X", self.Buffer[i])
		end

		printf("Sending -> %s", table.concat(hex, " "))
		self.Socket:send(table.concat(buffer, ""))
		self.Buffer = {}
		self.WriteP = 0
	end

	-- Constructor
	function SocketStream(socket)
		return Class({
			Socket = socket,
			Buffer = {},
			WriteP = 0
		}, mSocketStream)
	end

---------------------------------------
--                                   --
---------------------------------------
