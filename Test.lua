

local oldFormat = string.format

function string.format(self, ...)
	local args = {...}

	-- Loop over all instances of '%n$s'
	for itemS in self:gmatch("%%(%d+)$s") do
		-- Need convert string to number
		local itemN = tonumber(itemS)

		 -- Replace that part
		self = self:gsub("%%"..itemN.."$s", args[itemN] or "")
	end

	-- Use native format and return string
	return oldFormat(self, unpack(args))
end

print(string.format("%s %1$s %4$s", 1, 2, 3, 4, 5, 6))

