---------------------------------------
-- Hook lib                          --
---------------------------------------

	local hooks = {}
	Hook = {}

	-- Hook, Name, Callback
	function Hook.Add(h, n, f)
		if not hooks[h] then hooks[h] = {} end

		hooks[h][n] = f
	end

	-- Hook, Arguments
	function Hook.Call(h, ...)
		if not hooks[h] then return end

		local args = {...}
		for k,v in pairs(hooks[h]) do
			-- TODO: Use pcall/xpcall
			local ret = {v(unpack(args))}

			if #ret > 0 then return unpack(ret) end
		end
	end

	-- Hook, Name
	function Hook.Remove(h, n)
		if not hooks[h] then return end
		if not hooks[h][n] then return end

		hooks[h][n] = nil
	end

---------------------------------------
--                                   --
---------------------------------------
