---------------------------------------
-- Useful methods                    --
---------------------------------------

	sprintf = string.format
	function printf(text, ...)
		print(sprintf(text, ...))
	end

	-- Logger
	LOGGER_GENERAL = "[LWoW]"

	function Log(logger, text, ...)
		print(string.format("%s: "..text, logger, ...))
	end

	-- Misc
	function PrintTable(tab)
		for k,v in pairs(tab) do
			printf(" - %s = %s", tostring(k), tostring(v))
		end
	end

---------------------------------------
--                                   --
---------------------------------------
