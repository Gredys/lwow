---------------------------------------
-- Battle.NET Auth Server            --
---------------------------------------

	Server = {
		Clients = {},
	}

	function Server.OnLoad()
		Server.Socket = SocketStream(Socket.bind("*", Config.BNetPort))
			Server.Socket:SetTimeOut(0)
		Server.IP, Server.Port = Server.Socket:GetSockName()

		Log(LOGGER_GENERAL, "Server started at %s:%s", Server.IP, Server.Port)
	end

	function Server.OnUpdate(dt)
		-- Listen for new connections
		local Client = Server.Socket:Accept()
		if Client then
			Client:SetTimeOut(0)
				Client.IP, Client.Port = Client:GetPeerName()
			Log(LOGGER_GENERAL, "Client connected from %s:%s", Client.IP, Client.Port)
			Server.Clients[#Server.Clients + 1] = Client
		end

		-- Read all available data from client connections
		for _, Client in pairs(Server.Clients) do
			local Buffer, err = Client:ReadToBuffer()

			if Buffer then
				local Header = {
					Opcode = Buffer:Read(6),
					Channel = Buffer:Read(1) ~= 0 and Buffer:Read(4) or 0,
				}

				printf("Header -> [%s, %s]", Header.Opcode, Header.Channel)
				Hook.Call(sprintf("OnPacket::%s::%s", Header.Opcode, Header.Channel), Client, Buffer)
			elseif err then
				if err == "closed" then
					Log(LOGGER_GENERAL, "Client connection closed! (%s:%s)", Client.IP, Client.Port)
					table.remove(Server.Clients, _)
				end
			end
		end
	end

---------------------------------------
--                                   --
---------------------------------------

	Hook.Add("OnLoad", "Server::OnLoad", Server.OnLoad)
	Hook.Add("OnUpdate", "Server::OnUpdate", Server.OnUpdate)

---------------------------------------
--                                   --
---------------------------------------
