---------------------------------------
-- Includes                          --
---------------------------------------

	require("Config")

	require("Libraries.Socket")
	require("Libraries.Hook")
	require("Libraries.Helper")

	require("Modules.ClassLib")
	require("Modules.BitStream")
	require("Modules.SocketStream")

	for file in lfs.dir("Packets") do
		if file ~= "." and file ~= ".." then
			require("Packets." .. file:sub(1, #file - 4))
		end
	end

	-- Server
	require("BNetServer")

---------------------------------------
-- Main loop                         --
---------------------------------------

	local diff = os.clock()
	Log(LOGGER_GENERAL, "Starting main loop...")
	Hook.Call("OnLoad")
	while true do
		local tDiff = os.clock()
		if Hook.Call("OnUpdate", tDiff - diff) then
			Log(LOGGER_GENERAL, "Shuted down...")
			Hook.Call("OnExit")
			os.exit(1)
		end
		Socket.sleep(0.1)
		diff = tDiff
	end

---------------------------------------
--                                   --
---------------------------------------
